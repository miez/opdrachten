# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse

from .models import Opdracht
from .forms import OpdrachtForm


def index(request):
    latest_opdracht_list = Opdracht.objects.order_by('-datum')[:5]
    context = {'latest_opdracht_list': latest_opdracht_list}
    return render(request, 'opdrachtenapp/index.html', context)

def bewerk(request, opdracht_id):
    message = ""
    opdracht = False
    form = OpdrachtForm()

    try:
        opdracht = Opdracht.objects.get(pk=opdracht_id)
    except:
        opdracht = False
        message = 'Opdracht bestaat niet.'

    if request.method == "POST":
        form = OpdrachtForm(request.POST, instance = opdracht)
        if form.is_valid():
            form.save()
            message = "Succesvol bewerkt!"
        else:
            message = "Bewerkte opdracht niet opgeslagen"
    elif opdracht:
        form = OpdrachtForm(instance = opdracht)

    context = {'form':form, 'message':message}
    return render(request, 'opdrachtenapp/bewerk.html', context)

def toevoegen(request):
    form = OpdrachtForm()
    message = ""

    if request.method == "POST":
        form = OpdrachtForm(request.POST)
        if form.is_valid():
            form.save()
            message = "Succesvol toegevoegd!"
        else:
            message = "Opdracht niet opgeslagen"

    context = {'form':form, 'message':message}
    return render(request, 'opdrachtenapp/toevoegen.html', context)
