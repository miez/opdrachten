# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Opdracht

class OpdrachtAdmin(admin.ModelAdmin):
    list_display = ('titel', 'datum')


admin.site.register(Opdracht)
