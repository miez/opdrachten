# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Opdracht(models.Model):
    titel = models.CharField(max_length=200)
    datum = models.DateTimeField('date created')
    omschrijving = models.TextField()

    def __str__(self):
        return self.titel
