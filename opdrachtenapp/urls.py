from django.urls import path

from . import views

app_name = 'opdrachtenapp'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:opdracht_id>/', views.bewerk, name='bewerk'),
    path('toevoegen', views.toevoegen, name='toevoegen'),
]
