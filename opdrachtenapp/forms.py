from django.forms import ModelForm
from django import forms
from .models import Opdracht

class DateInput(forms.DateInput):
    format_key = 'DATE_INPUT_FORMATS'
    input_type = 'date'

class OpdrachtForm(ModelForm):
    class Meta:
        model = Opdracht
        fields = '__all__'
        labels = {
            'datum': 'Einddatum opdracht'
        }
        widgets = {
            'datum': DateInput(attrs={'type': 'date', 'format' : '%d-%m-%Y'}),
        }
